#include "opencv2/opencv.hpp"
#include <string>
#include <iostream>

using namespace std;
using namespace cv;

void graph(Mat &histogram, Mat &image, Scalar color = Scalar(255, 255, 255), int bins = 256, int histWidth = 512, int histHeight = 400){
    int binWidth = cvRound( (double) histWidth/bins);
    normalize(histogram, histogram, 0, image.rows, NORM_MINMAX, -1, Mat() );
    for( int i = 1; i < bins; i++ ) {
        line(image, Point(binWidth * i, histHeight),
             Point(binWidth * i, histHeight - cvRound(histogram.at<float>(i))),
             color, 2, 8, 0);
    }
    return;
}

int main( int argc, char* argv[] ) {
	//load the image
	Mat img = imread("../lab3_image.jpg");

    cout << "Image loaded" << endl;
	int bins = 256;
    int histWidth = 512;
    int histHeight = 400;
    vector<Scalar> colors = {Scalar( 255, 0, 0), Scalar( 0, 255, 0),Scalar( 0, 0, 255)};


    vector<Mat> histograms(3);
	vector<Mat> histImages(3) ;

    for (int i = 0; i < 3; i++) {
        Mat emptyHist(400, 512, CV_8UC3, Scalar(0, 0, 0));
        histImages[i].push_back(emptyHist);

    }
    Mat splitImg[3];
	split(img, splitImg);

	float range[] = {0, float(bins-1)};
	const float* histRange = {range};

    Mat histWindow = Mat::zeros( histHeight, 3 * histWidth, CV_8UC3);

    for (int i = 0; i < 3; i++){
		calcHist( &splitImg[i], 1, 0, Mat(), histograms[i], 1, &bins, &histRange);
        graph( histograms[i], histImages[i], colors[i]);
        histImages[i].copyTo( histWindow( Rect( i * histImages[i].size().width, 0, histImages[i].size().width, histImages[i].size().height)));
	}

	imshow("histos", histWindow);
    waitKey(0);


	for( int i = 0; i < 3; i++){
		equalizeHist(splitImg[i],splitImg[i]);
		calcHist(&splitImg[i], 1, 0, Mat(), histograms[i], 1, &bins, &histRange);
        graph( histograms[i], histImages[i], colors[i]);
        histImages[i].copyTo( histWindow( Rect( i * histImages[i].size().width, 0, histImages[i].size().width, histImages[i].size().height)));
    }
	
	Mat eqImg;
	merge(splitImg, 3, eqImg);
	//imshow("image", eqImg);
	imshow("histos", histWindow);
	
	waitKey(0);

 	Mat labImg;
	cvtColor(img, labImg, CV_BGR2Lab);
	
	Mat labSplit[3]; 
	split(labImg, labSplit);
	Mat lHist;
	Mat lHistImage(400, 512, CV_8UC3, Scalar(0, 0, 0));
	equalizeHist(labSplit[0],labSplit[0]);
	calcHist(&labSplit[0], 1, 0, Mat(), lHist, 1, &bins, &histRange);
    graph( lHist, lHistImage, Scalar(255,255,255));
    merge(labSplit, 3, labImg);
    imshow("histos",lHistImage);
    cvtColor(img, labImg, CV_Lab2BGR);
    imshow("image",img );

    waitKey(0);
    return 0;
}
